﻿using System;
using System.Windows.Forms;

namespace JocPiticoT
{
    partial class Form11
    {
        private const string V = "Form1";

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStart = new System.Windows.Forms.Button();
            this.PlayerName = new System.Windows.Forms.Label();
            this.playerPosition = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(654, 356);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(140, 65);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "START";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // PlayerName
            // 
            this.PlayerName.AutoSize = true;
            this.PlayerName.Location = new System.Drawing.Point(443, 216);
            this.PlayerName.Name = "PlayerName";
            this.PlayerName.Size = new System.Drawing.Size(132, 20);
            this.PlayerName.TabIndex = 1;
            this.PlayerName.Text = "playerNameLabel";
            // 
            // playerPosition
            // 
            this.playerPosition.AutoSize = true;
            this.playerPosition.Location = new System.Drawing.Point(441, 271);
            this.playerPosition.Name = "playerPosition";
            this.playerPosition.Size = new System.Drawing.Size(146, 20);
            this.playerPosition.TabIndex = 2;
            this.playerPosition.Text = "playerPositionLabel";
            this.playerPosition.Click += new System.EventHandler(this.label2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.playerPosition);
            this.Controls.Add(this.PlayerName);
            object p = this.Controls.Add(this.btnStart);
            this.PlayerName = V;
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void SuspendLayout()
        {
            throw new NotImplementedException();
        }

        private void ResumeLayout(bool v)
        {
            throw new NotImplementedException();
        }

        private void PerformLayout()
        {
            throw new NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.Button btnStart;


        private System.Windows.Forms.Label PlayerName;
        private System.Windows.Forms.Label playerPosition;
    }
}

