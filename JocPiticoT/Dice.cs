﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JocPiticoT
{
    internal class Dice
    {
        static Random r = new Random();

        public static int Roll()
        {
            return r.Next(1, 7);
        }
    }
}
