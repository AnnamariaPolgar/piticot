﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JocPiticoT
{
    internal class PiticotGame
    {
        public Board board = new Board();
        public List<Player> players = new List<Player>();
        public Dice dice = new Dice();
        public Form1 f;
        internal void Start(Form1 f)
        {
            players.Add(new Player { name = "PionPropriu", position = 0 });
            players.Add(new Player { name = "PionAdversar", position = 0 });

            board.Show(f);

            this.f = f;

            PlayGame();
        }

        private void PlayGame()
        {
            int currentPlayerIndex = 0;
            Player player = players[currentPlayerIndex];

            while (!Winner(player))
            {
                int dice = Dice.Roll();
                HidePlayer(player);
                UpdatePlayer(player, dice);
                ShowPlayer(player);

                currentPlayerIndex = NextPlayerIndex(currentPlayerIndex);
                player = players[currentPlayerIndex];

                f.Refresh();
                //Thread.Sleep(1000);
            }
        }

        private int NextPlayerIndex(int currentPlayerIndex)
        {
            currentPlayerIndex++;
            return currentPlayerIndex % players.Count;
        }

        private void ShowPlayer(Player player)
        {
            f.ShowPlayer(player);
        }

        private void UpdatePlayer(Player player, int dice)
        {
            player.position += dice;

            if (player.position == 5)
            {

            }
            if (player.position == 10)
            {
                player.position = 1;
            }
            if (player.position == 14)
            {
                player.position = 17;
            }
            if (player.position == 18)
            {
                player.position = 15;
            }
            if (player.position == 24)
            {
                player.position = -1000;
            }
            if (player.position == 28)
            {
                player.position = 27;
            }
            if (player.position == 33)
            {
                player.position = 40;
            }
            if (player.position == 39)
            {
                player.position = 34;
            }
            if (player.position == 43)
            {
                player.position = 46;
            }
            if (player.position == 47)
            {
                player.position = 46;
            }
            if (player.position == 52)
            {

            }
            if (player.position == 55)
            {
                player.position = 51;
            }
            if (player.position == 58)
            {
                player.position = 61;
            }
            if (player.position == 60)
            {

            }
            if (player.position == 64)
            {
                player.position = 65;
            }
            if (player.position == 65)
            {
                MessageBox.Show("A castigat jocul");
            }
        }

        private void HidePlayer(Player player)
        {
            f.HidePlayer(player);
        }

        private bool Winner(Player player)
        {
            return true;
            //  return players.Where(x => x.money > 0).Count() == 1; // un singur jucator a ramas cu suma peste 0
        }
    }
}