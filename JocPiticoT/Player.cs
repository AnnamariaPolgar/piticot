﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JocPiticoT
{
    internal class Player
    {
        public string name;
        public int position;

        public Player()
        {
            name = "Default";

            position = 0;
        }
        public Player(string name, int position)
        {
            this.name = name;

            position = 0;
        }

        public void Show(startingForm f)
        {
            f.ShowPlayer(this);
        }

    }
}
